    import { state, Respond } from '@redstu/redstu-js';
    import { Counter } from './util.js';

    let petIdCounter = new Counter(state.counters.petId);
    let orderIdCounter = new Counter(state.counters.orderId);
    let userIdCounter = new Counter(state.counters.userId);
    let sessionIdcounter = new Counter(state.counters.sessionId);

    //List pets
    Respond.GET("/pet", (req, resp) => {
        resp.sendObject(state["pet"]);
    });

    //Add new pet
    Respond.POST("/pet",  async (req, resp) => {
        let object = await req.body.object();
        let pet = { category : object.category, name: object.name, photoUrls: object.photoUrls, tags: object.tags, status: object.status};
        pet.id = petIdCounter.increment();
        state["pet"].push(pet);
        resp.sendObject(pet);
    });

    //Update an existing pet
    Respond.PUT("/pet", async (req, resp) => {
        let object = await req.body.object();
        let pet = { id: object.id, category : object.category, name: object.name, photoUrls: object.photoUrls, tags: object.tags, status: object.status};
        state["pet"].splice(state["pet"].findIndex(item => item.id === pet.id), 1);
        state["pet"].push(pet);
        resp.sendObject(pet);

    });

    //Find pets by status
    Respond.GET("/pet/findByStatus/{status}", async (req, resp) => {
        let status = req.pathParams["status"].string();
        let filteredPet = state["pet"].filter(it => it.status===status);
        resp.sendObject(filteredPet);
    });


    //Get pet by Id
    Respond.GET("/pet/{id}", async (req, resp) => {
        let petId = req.pathParams["id"].number();
        let filtered = state["pet"].filter(it => it.id===petId);
        if(filtered.length)
            resp.sendObject(filtered[0]);
        else {
            resp.setStatus(404);
            resp.send();
        }
    });

    // Delete pet by id
    Respond.DELETE("/pet/{id}", async (req, resp) => {
        let orderId = req.pathParams["id"].number();
        state["pet"].splice(state["pet"].findIndex(item => item.id === orderId), 1);
        resp.sendObject();
    });

    //Place an order for a pet
    Respond.POST("/store/order", async (req, resp) => {
        let object = await req.body.object();
        let order = { petId : object.petId, quantity: object.quantity, shipDate: object.shipDate, status: object.status, complete: object.complete};
        order.id = orderIdCounter.increment();
        state["order"].push(order);
        resp.sendObject(order);
    });

    //Find order by id
    Respond.GET("/store/order/{id}", async (req, resp) => {
        let orderId = req.pathParams["id"].number();
        let filtered = state["order"].filter(it => it.id===orderId);
        if(filtered.length)
            resp.sendObject(filtered[0]);
        else {
            resp.setStatus(404);
            resp.send();
        }
    });

    // Delete order by id
    Respond.DELETE("/store/order/{id}", async (req, resp) => {
        let orderId = req.pathParams["id"].number();
        state["order"].splice(state["order"].findIndex(item => item.id === orderId), 1);
        resp.sendObject();
    });


    // Get store inventory
    Respond.GET("/store/inventory", async (req, resp) => {
        let sold = state["pet"].filter(p => p.status === "sold"); //return array of sold
        let available = state["pet"].filter(p => p.status === "available"); //return array of available
        let pending = state["pet"].filter(p => p.status === "pending"); //return array of pending

        const inventory = {
            sold: sold.length,
            available: available.length,
            pending: pending.length
        }

        resp.sendObject(inventory);
    });

    // Creates list of users with given input array
    Respond.POST("/user/createWithArray", async (req, resp) => {
        let array = await req.body.array();
        for (const object of array){
            let user = { username : object.username, firstName: object.firstName, lastName: object.lastName, email: object.email, password: object.password, phone: object.phone, userStatus: object.userStatus};
            user.id = userIdCounter.increment();
            state["user"].push(user);
        }
        resp.sendObject(state["user"]);
    });

    // Creates list of users with given input array
    Respond.POST("/user/createWithList", async (req, resp) => {
        let object = await req.body.array();
        for (const object of object){
            let user = { username : object.username, firstName: object.firstName, lastName: object.lastName, email: object.email, password: object.password, phone: object.phone, userStatus: object.userStatus};
            user.id = userIdCounter.increment();
            state["user"].push(user);
        }
        resp.sendObject(state["user"]);
    });

    // Get user by username
    Respond.GET("/user/{username}", async (req, resp) => {
        let username = req.pathParams["username"].string();
        let filtered = state["user"].filter(it => it.username===username);
        if(filtered.length)
            resp.sendObject(filtered[0]);
        else {
            resp.setStatus(404);
            resp.send();
        }
    });

    // Update user
    Respond.PUT("/user/{username}", async (req, resp) => {
        let username = req.pathParams["username"].string();
        let object = await req.body.object();
        let user = { id: object.id, username : object.username, firstName: object.firstName, lastName: object.lastName, email: object.email, password: object.password, phone: object.phone, userStatus: object.userStatus};
        state["user"].splice(state["user"].findIndex(it => it.username === username), 1);
        state["user"].push(user);
        resp.sendObject(user);
    });

    // Delete user
    Respond.DELETE("/user/{username}", async (req, resp) => {
        let username = req.pathParams["username"].string();
        state["user"].splice(state["user"].findIndex(item => item.username === username), 1);
        resp.sendObject();
    });

    // Create user
    Respond.POST("/user", async (req, resp) => {
        let object = await req.body.object();
        let user = {username : object.username, firstName: object.firstName, lastName: object.lastName, email: object.email, password: object.password, phone: object.phone, userStatus: object.userStatus};
        user.id = userIdCounter.increment();
        state["user"].push(user);
        resp.sendObject(state["user"]);
    });

    // Logs user into the system
    Respond.GET("/user/login", async (req, resp) => {
        let user = await req.body.object();
        let username = user.username;
        let sessionId = sessionIdcounter.increment();
        let responseMessage = {
            code: 200,
            type: "unknown",
            message: "logged in user session: " + sessionId
        };
        let filtered = state["user"].filter(it => it.username===username);
        if(filtered.length){
            const session = {"id": sessionId, "username": filtered[0].username};
            state["session"].push(session);
            resp.sendObject(responseMessage);
        } else {
            resp.setStatus(404);
            resp.send();
        }
    });

    // Logs out current logged in user session
    Respond.GET("/user/logout", async (req, resp) => {
        let responseMessage = {
            "code": 200,
            "type": "unknown",
            "message": "ok"
        };
        resp.setStatus(200);
        resp.sendObject(responseMessage);
    });